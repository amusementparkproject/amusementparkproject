import {useState} from 'react'
import axios from 'axios'
import { Input, FormGroup, Label, Col, Form, Button } from 'reactstrap'
import { useNavigate} from 'react-router-dom'


function Login() {
  
  let loginForm={
  border:'1px solid black', 
  width:'500px',
  padding:'1rem',
  margin:'3rem auto',
  borderRadius:'3px',
  backgroundColor:'pink'
  }
  if(window.innerWidth>600){
    loginForm.width='40%'
    loginForm.margin='2rem auto'
  }
  if(window.innerWidth<=600){
    loginForm.width='40%'
    loginForm.margin='2rem auto'
  }
  const navigate=useNavigate();
    let [formdata, setFormdata] = useState({
        email: '',
        password: '',
        check: 'false'
    
      })
      const handleOnChage = (e) => {
        let { name, value, type, checked } = e.target;
        let inputValue = type === 'checkbox' ? checked : value
        setFormdata({
          ...formdata,
          [name]: inputValue
        })
      }
      const handlesubmit = (e) => {
        e.preventDefault();
        console.log(formdata)
        login();
      }
    
      let login = async () => {
        try {
          let response = await axios.post("http://localhost:3004/login", formdata);
          console.log(response)
          if(response.data.ok===1){
            alert("login sucessful")
            setFormdata({
             
              email:'',
              password:'',
             
              check:'false'
          
            })
          }
          navigate('/')
        }
        catch (err) {
          console.log(err);
        }
      }
    
      return (
      
       
          <div style={loginForm}>
            <Form onSubmit={handlesubmit}>
                <Col md={6}>
                  <FormGroup>
                    <h3 style={{margin:'1rem auto'}}>Login Form</h3>
                    <Label for="exampleEmail">
                      Email
                    </Label>
                    <Input
                      id="exampleEmail"
                      name="email"
                      placeholder="email"
                      type="email"
                      value={formdata.email}
                      onChange={handleOnChage}
                    />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="examplePassword">
                      Password
                    </Label>
                    <Input
                      id="examplePassword"
                      name="password"
                      placeholder="password"
                      type="password"
                      value={formdata.password}
                      onChange={handleOnChage}
                    />
                  </FormGroup>
                </Col>
              <FormGroup check>
                <Input
                  id="exampleCheck"
                  name="check"
                  type="checkbox"
                  value={formdata.checkbox}
                  onChange={handleOnChage}
                />
                <Label
                  check
                  for="exampleCheck"
                >
                  Check me out
                </Label>
              </FormGroup>
              <Button  type='submit' style={{backgroundColor:'blue'}}>
                Login
              </Button>
            </Form>


    </div>
    
  )
}

export default Login