
import './App.css';
import Navbar from './Components/Navbar';
import { BrowserRouter,Routes,Route } from 'react-router-dom';

import About from './Pages/About';
import Rides from './Pages/Rides';
import TimingTickets from './Pages/TimingTickets';
import Login from './Pages/Login';

import Register from './Pages/Register';
import Footer from './Pages/Footer'

import HTrides from './Components/HTrides';
import Landrides from './Components/Landrides';
import KidsRides from './Components/Kidrides';
import Water from './Components/Water';
import Offers from './Pages/Offers';
import Booknow from './Components/Booknow';


function App() {
  return (
    <div>
      <BrowserRouter>
      <Navbar/>
     
      <Routes>
      <Route path='/' element={<About />} />
      <Route path='Rides' element={<Rides/>} />
      <Route path='TimingTickets' element={<TimingTickets/>} />
      <Route path='Register' element={<Register/>} />
      <Route path='/Login' element={<Login />} />
      <Route path='/Booknow' element={<Booknow />} />


      <Route path='/HighthrillRides' element={<HTrides />} />
      <Route path='/LandRides' element={<Landrides />} />
      <Route path='/WaterRides' element={<Water />} />
      <Route path='KidsRides' element={<KidsRides />} />
      <Route path='/Offers' element={<Offers/>} />

      </Routes>
       </BrowserRouter>
       <Footer />
     
 
    </div>
  );
}

export default App;
