import React from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import WONDERLA from '../Images/WONDERLA.jpg'
import img2 from '../Images/img2.jpeg';
import img3 from '../Images/img3.jpg';

function Caurosel() {
  const carouselStyles = {
    height:'70vh',
    margin: " 20px 0px",
  };
 
 
  return (
    <div>
      <div>
        <Carousel
          autoPlay
          interval="3000"
          transitionTime="2500"
          showThumbs={false}
          infiniteLoop={true}
        >
          <div style={carouselStyles}>
            <img
              src={WONDERLA}
              alt=""
            />
          </div>
          <div style={carouselStyles }>
            <img
              src={img2}
              alt=""
            />
          </div>
          <div style={carouselStyles}>
            <img
              src={img3}
              alt=""
            />
          </div>
        </Carousel>
      </div>
    </div>
  );
}

export default Caurosel